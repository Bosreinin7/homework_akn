//
//  ViewController.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/23/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate  {
    
    @IBOutlet weak var articleTableView: UITableView!
    var refreshControl:UIRefreshControl?
    var articlePresenter:ArticlePresenter?
    var articles = [Article]()
    var n = 1
    var newFetchBool = 0
    var indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        self.articleTableView?.addSubview(refreshControl!)
        self.articlePresenter = ArticlePresenter()
        self.articlePresenter?.delegate = self
        self.articlePresenter?.getArticle(page: 1, limit: 15)
        self.articleTableView.layer.backgroundColor = UIColor.lightGray.cgColor
        
        // set up the refresh control
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
    }
    @objc func refreshData() {
        n = 1
        self.articlePresenter?.getArticle(page: n, limit: 15)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let height = scrollView.frame.size.height
        let contentYOffet = scrollView.contentOffset.y
        let distanceFromButtom = scrollView.contentSize.height - contentYOffet
        
        if distanceFromButtom < height {
            self.articleTableView.layoutIfNeeded()
            n = n + 1
            self.articleTableView.tableFooterView = indicatorView
            self.articleTableView.tableFooterView?.isHidden = false
            self.articleTableView.tableFooterView?.center = indicatorView.center
            self.indicatorView.startAnimating()
            self.articlePresenter?.getArticle(page: n, limit: 15)
            newFetchBool = 0
        }else if !decelerate {
            newFetchBool = 0
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        newFetchBool = 0
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        newFetchBool = newFetchBool + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (articles.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("articlesTableViewCell", owner: self, options: nil)?.first as! articlesTableViewCell
        cell.images?.kf.setImage(with:URL(string: articles[indexPath.row].image ?? ""), placeholder:#imageLiteral(resourceName: "calendar"))
        cell.descLabel.text = articles[indexPath.row].desc
        cell.dateLabel.text = articles[indexPath.row].date?.formatDate(getTime: false)
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("indexPath\(indexPath.row)")
        //go to second sotryboard
        let second = self.storyboard?.instantiateViewController(withIdentifier: "detailStoryboard") as! SecondViewController
        
        second.Title = articles[indexPath.row].title
        second.descript = articles[indexPath.row].desc
        second.image = articles[indexPath.row].image
        second.date = articles[indexPath.row].date?.formatDate(getTime: false)
               self.navigationController?.pushViewController(second, animated: true)
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            let article = self.articles[indexPath.row]
            self.performSegue(withIdentifier: "addEditArticle", sender: article)
        }
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let id = self.articles[indexPath.row].id
            self.articlePresenter?.deleteArticle(id: id!)
            self.articles.remove(at: indexPath.row)
            self.articleTableView.reloadData()
            print("Before\(String(describing: self.articles.count))")
            
        }
        return [edit, delete]
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addEditArticle" {
            let dest = segue.destination as! AddEditViewController
            dest.detailArticle = sender as? Article
        }
    }

}
extension ViewController:ArticlePresenterProtocol{
   
    
    func responseArticle(_ article: [Article]) {
        if n == 1 {
            self.articles.removeAll()
            self.refreshControl?.endRefreshing()
        }
        self.articles = self.articles + article
       
        DispatchQueue.main.async {
            self.articleTableView.reloadData()
            self.indicatorView.stopAnimating()
        }
    }
    func responseMessage(msg: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: msg, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
extension String {
    func subString(startIndex: Int, endIndex: Int) -> String {
        let end = (endIndex - self.count) + 1
        let indexStartOfText = self.index(self.startIndex, offsetBy: startIndex)
        let indexEndOfText = self.index(self.endIndex, offsetBy: end)
        let substring = self[indexStartOfText..<indexEndOfText]
        return String(substring)
    }
    
    func formatDate(getTime: Bool) -> String {
        if getTime {
            return "\(self.subString(startIndex: 6, endIndex: 7))-\(self.subString(startIndex: 4, endIndex: 5))-\(self.subString(startIndex: 0, endIndex: 3)) / \(self.subString(startIndex: 8, endIndex: 9)):\(self.subString(startIndex: 10, endIndex: 11))"
        } else {
            return "\(self.subString(startIndex: 6, endIndex: 7))-\(self.subString(startIndex: 4, endIndex: 5))-\(self.subString(startIndex: 0, endIndex: 3))"
        }
    }
}

