//
//  AddEditViewController.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/26/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class AddEditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    var articlePresenter:ArticlePresenter?
    var detailArticle:Article?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.articlePresenter = ArticlePresenter()
        self.articlePresenter?.delegate = self
        
        if detailArticle != nil {
            self.titleTextField.text = detailArticle?.title
            self.descriptionTextView.text = detailArticle?.desc
            if let url = URL(string: (detailArticle?.image)!) {
                self.articleImageView.kf.setImage(with: url)
            }
        }
    }
    @IBAction func saveButton(_ sender: Any) {
        let image = UIImageJPEGRepresentation(self.articleImageView.image!, 0.5)
        let article = Article()
        if detailArticle != nil {
            article.id = detailArticle?.id
        }else{
            article.id = 0
        }
        article.title = self.titleTextField.text
        article.desc = self.descriptionTextView.text
        
        self.articlePresenter?.insertUpdateArticle(article: article, img: image!)
        
    }
    @IBAction func browseImage(_ sender: UITapGestureRecognizer){
     
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        DispatchQueue.main.async {
            self.articleImageView.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension AddEditViewController:ArticlePresenterProtocol {
    func responseMessage(msg: String) {
        print("hjhgjgjhgkgh=========\(msg)")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Success", message: msg, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func responseArticle(_ articles: [Article]) {}
    
}


