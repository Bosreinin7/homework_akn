//
//  articlesTableViewCell.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/23/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit
import Kingfisher
class articlesTableViewCell: UITableViewCell {

    @IBOutlet weak var images: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    var Image:String?
    var desce:String?
    var Date:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        saveButton.backgroundColor = UIColor.red
        saveButton.layer.cornerRadius = 15
        saveButton.layer.borderWidth = 1
        saveButton.layer.borderColor = UIColor.white.cgColor
     

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}



