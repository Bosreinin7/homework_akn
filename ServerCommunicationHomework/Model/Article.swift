//
//  articles.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/23/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation
import ObjectMapper
class Article:Mappable{
    var id:Int?
    var title:String?
    var desc:String?
    var date:String?
    var category:Category?
    var image:String?
    init() {
        self.category = Category()
    }
    required init?(map: Map) {}
    func mapping(map: Map) {
        id <- map["ID"]
        title <- map["TITLE"]
        desc <- map["DESCRIPTION"]
        category <- map["CATEGORY"]
        image <- map["IMAGE"]
        date <- map["CREATED_DATE"]
        
    }

    
}
class Category:Mappable{
    var id:Int?
    var name:String?
    init() {
        
    }
    required init?(map: Map) {}
    func mapping(map: Map) {
        id   <- map["ID"]
        name  <- map["NAME"]
    }
    
    
}
