//
//  ArticleService.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/25/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation
import  Alamofire
import  SwiftyJSON
import ObjectMapper

class ArticleService{
    var article_get_url = "http://api-ams.me/v1/api/articles"
    var article_post_url = "http://api-ams.me/v1/api/articles"
    var article_upload_image_url = "http://api-ams.me/v1/api/uploadfile/single"
    var article_put_url = "http://174.138.20.101:15000/v1/api/articles/"
    var article_delete_url = "http://174.138.20.101:15000/v1/api/articles/"
    let headers = [
        "Content-Type":"application/json",
        "Accept":"application/json",
        "Authorization":"Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
    ]
    var delegate:ArticleServiceProtocol?
    
    
    //===============get Article==================
    func getArticle(page:Int,limit:Int){
      var articles=[Article]()
        Alamofire.request("\(article_get_url)?page=\(page)&limit=\(limit)", method: .get, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if let json = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any] {
                    let objects = json["DATA"] as! NSArray
                    for obj in objects {
                        articles.append(Article(JSON: obj as! [String:Any])!)
                    }
                    self.delegate?.responseArticle(articles)
                }
            }
        }
    }
    //==============insertUpdate / Uploadimage=======
    func insertUpdate(article:Article,image:Data) {
        
        Alamofire.upload(multipartFormData: { (multipart) in
            multipart.append(image, withName: "FILE", fileName: ".jpg", mimeType: "image/jpeg")
        }, to: article_upload_image_url,method:.post,headers:headers) { (encodingResult) in
            switch encodingResult {
            case .success(request: let upload, streamingFromDisk: _ , streamFileURL: _):
                upload.responseJSON(completionHandler: { (response) in
                    if let data = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any] {
                        let image_url = data["DATA"] as! String
                        article.image = image_url
                        if article.id == 0 {
                            self.insertArticle(article: article)
                        }else {
                            self.updateArticle(article: article)
                        }
                    }
                })
            case .failure(let error):
                print(error)
            }
        }

    }
    //===========Post Article==================
    func insertArticle(article:Article)  {
        let newData:[String:Any] = [
            "TITLE": article.title!,
            "DESCRIPTION": article.desc!,
            "AUTHOR": 1,
            "CATEGORY_ID": 1,
            "STATUS":"1",
            "IMAGE": article.image!
            ]
        Alamofire.request(article_post_url, method: .post, parameters: newData, encoding: JSONEncoding.default, headers: ["Authorization":"Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]).responseJSON { (response) in
            if response.result.isSuccess {
                self.delegate?.responseMesssage("Insert Successfully!")
            }
        
       }
    }
    
    //=============Update Article======================
    func updateArticle(article:Article) {
        let newData:[String:Any] = [
            "TITLE": article.title!,
            "DESCRIPTION": article.desc!,
            "AUTHOR": 1,
            "CATEGORY_ID": 1,
            "STATUS": "1",
            "IMAGE": article.image!
        ]
        
        Alamofire.request("\(article_put_url)\(article.id!)", method: .put, parameters: newData, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                self.delegate?.responseMesssage("Update Successfully!")
            }
        }
    }
 //===============Delete Article=======================
     func deleteArticle(id:Int) {
        Alamofire.request("\(article_delete_url)\(id)", method: .delete, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                self.delegate?.responseMesssage("Delete Successfully!")
                
            }
        }
    }
    
}
