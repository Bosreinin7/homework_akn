//
//  ServiceProtocol.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/24/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation
protocol ArticleServiceProtocol {
    func responseArticle(_ article:[Article])
    func responseMesssage(_ msg:String)
}
