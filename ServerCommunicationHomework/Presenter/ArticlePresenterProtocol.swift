//
//  ArticlePresenterProtocol.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/25/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation

protocol ArticlePresenterProtocol {
    func responseArticle(_ article:[Article])
    func responseMessage(msg:String)
}
