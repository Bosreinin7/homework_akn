//
//  ArticlePresenter.swift
//  ServerCommunicationHomework
//
//  Created by SreiNin Bo on 12/25/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import Foundation

class ArticlePresenter {
    var delegate:ArticlePresenterProtocol?
    var articleService:ArticleService?
    init() {
        self.articleService = ArticleService()
        self.articleService?.delegate = self
           }
    func getArticle(page:Int,limit:Int){
        self.articleService?.getArticle(page: page, limit: limit)
    }
    func insertUpdateArticle(article:Article, img:Data) {
        self.articleService?.insertUpdate(article: article, image: img)
    }
    func deleteArticle(id:Int) {
        self.articleService?.deleteArticle(id: id)
    }
    
}
extension ArticlePresenter:ArticleServiceProtocol{
    func responseMesssage(_ msg: String) {
        self.delegate?.responseMessage(msg: msg)
    }
    func responseArticle(_ article: [Article]) {
    self.delegate?.responseArticle(article)
    }
    
    
}
